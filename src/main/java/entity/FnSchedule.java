package entity;
import dao.DAO;
import dao.Identifable;
import dao.ScheduleDao;


public class FnSchedule  {


    public  static int getMaxId(DAO<ScheduleLine> dao){
        return  dao.getAll().stream()
                .map(b -> b.id)
                .max((id1, id2)-> id1 - id2)
                .orElse(0);
    }
}

