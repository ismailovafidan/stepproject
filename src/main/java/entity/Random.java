package entity;

import java.util.stream.IntStream;


public  class Random {
  static  java.util.Random random = new java.util.Random();
    public static  int getRandomNumber(){
        return IntStream.generate(()->(int)(Math.random()*100)+50).limit(1).findAny().getAsInt();
    }

    public static  int getRandomCity(){
        return random.nextInt(Cities.values().length-1);
    }




}
