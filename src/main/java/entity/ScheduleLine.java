package entity;

import dao.Database;
import dao.Identifable;
import java.io.Serializable;
import java.time.LocalDateTime;

public class ScheduleLine implements Serializable, Identifable {

    int id;
    static int counter = FnSchedule.getMaxId(new Database().dbSchedule);
    FlightNumber FN;
    LocalDateTime time;
    Cities origin;
    Cities destination;

    public ScheduleLine(int id,LocalDateTime time, Cities origin, Cities destination) {
        this.id=id;
        this.time = time;
        this.origin = origin;
        this.destination = destination;
    }

    int freeSeat;
    private static long serialVersionUID = 1L;

    {
        counter = FnSchedule.getMaxId(new Database().dbSchedule);
    }

    @Override
    public int getId() {
        return id;
    }
    public ScheduleLine() {
        this.id =++counter;
        this.FN = new FlightNumber();
        this.time = Utilities.generate();
        this.origin = Utilities.getRandomCity();
        do {
            this.destination = Utilities.getRandomCity();
        } while (this.origin == this.destination);
        this.freeSeat = Random.getRandomNumber();
    }

    public LocalDateTime getTime() {
        return time;
    }

    public String getOriginString(){
        return  origin.get();
    }


    public  String getDestinationString(){
        return destination.get();
    }



    public String parseTime() {
        String[] arr = this.getTime().toString().split("T");
        return arr[0];
      }

    @Override
    public String toString() {
        return "ID:"+id+"-"+FN+" airport: "+
                " " + origin +
                " | " + destination +"| "+time+
                " | "
                + freeSeat+" seats";

    }
}
