package entity;

import java.time.LocalDateTime;
import java.util.*;

public class Utilities {

public  static Cities getRandomCity(){
        return Cities.values()[Random.getRandomCity()];
    }

    public static LocalDateTime generate(){
        LocalDateTime localDateTime = LocalDateTime.now();
        return localDateTime.plusHours(Random.random.nextInt(100));
}

       public static String getInfo(String text){
        Scanner in = new Scanner(System.in);
        Optional <String> info = Optional.empty();
        while((!info.isPresent()) || !ValidationCheck.lengthNameValid(info.get())){
            System.out.println("Please, enter your "+ text+ ": ");
            info = Optional.of(in.next());

        }
        return info.get();
    }

    public static List<Passenger> checkTicketSize(String ticketSize){
        List<Passenger> passengers=new ArrayList<>();
        for (int i = 0; i <Integer.parseInt(ticketSize) ; i++) {
            String passName=getInfo("Passenger name");
            String passSurname=getInfo("Passenger Surname");
            passengers.add(new Passenger(i,passName,passSurname));

  }
        return passengers;
    }

    public static void showSchedule(List<ScheduleLine> list){
        if(list.isEmpty()) System.out.println("There is no any flight");
        else
            for (ScheduleLine schedule: list) {
                System.out.println(schedule.toString());
            }
    }

    public static void showMySchedule(HashMap<Booking,ScheduleLine> list){
        if(list.isEmpty()) System.out.println("There is no any flight");
        else System.out.println(list.toString());
}
    }

