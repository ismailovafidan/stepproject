package entity;

import dao.Database;
import dao.Identifable;
import java.io.Serializable;


public class User implements Serializable, Identifable {
    static int counter = FnUser.getMaxId(new Database().dbUser);
    int id;
    String name;
    String Surname;
    String username;
    String password;
    private static long serialVersionUID = 1L;

    {
        counter = FnUser.getMaxId(new Database().dbUser);
    }


    @Override
    public int getId() {
        return id;
    }

    public User(int id,String name, String surname) {
        this.id=id;
        this.name = name;
        Surname = surname;
    }

    public User(String name, String surname, String username, String password) {
        this.id = ++counter;
        this.name = name;
        Surname = surname;
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

     public String getPassword() {
        return password;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!( obj instanceof User)) return false;
        if(this.id == ((User)obj).id) return true;
        return false;
    }

    @Override
    public String toString() {
        return String.format("%3s ,%10s ,%15s ,%10s ,%12s",this.id, this.name, this.Surname, this.username, this.password);
    }
}
