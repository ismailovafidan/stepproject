package entity;

import dao.Identifable;

import java.io.Serializable;

public class Passenger implements Serializable, Identifable {

    private int id;
    private String passName;
    private String passSurname;

    public Passenger(int id, String passName, String passSurname) {
        this.id = id;
        this.passName = passName;
        this.passSurname = passSurname;
    }

    public Passenger(String passName, String passSurname) {
        this.passName = passName;
        this.passSurname = passSurname;
    }

    @Override
    public int getId() {
        return id;
    }


    @Override
    public String toString() {
        return
                "Passengers: " + id +
                "|" + passName+
                "|" + passSurname +"\n";
    }
}
