package entity;

import java.io.PrintStream;

public class Print {
    PrintStream printStream = new PrintStream(System.out);

    public void printMain() {
        printStream.println("========================");
        printStream.println("Welcome to Booking App!!");
        printStream.println("========================");
        printStream.println("1-Register");
        printStream.println("2-Login");
        printStream.println("3-Show Schedule");
        printStream.println("0-Exit");
        printStream.println("-------------------------");


    }

    public void Goodbye() {
        printStream.println("GoodBye! See You Again");
        System.exit(0);
    }


    public void printLoginMenu() {
        System.out.println("-------------");
        System.out.println("1-Show Schedule");
        System.out.println("2-Booking");
        System.out.println("3-My Flight");
        System.out.println("4-Cancel my Flight");
        System.out.println("5-exit");
    }
}
