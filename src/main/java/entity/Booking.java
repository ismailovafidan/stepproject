package entity;

import dao.Database;
import dao.Identifable;
import java.io.Serializable;
import java.util.List;


public class Booking implements Serializable, Identifable {
    static  int counter;
    private int id;
    private int userId;
    private int scheduleLineID;
    private int ticketSize;
    List<Passenger> passengerList;
    private static long serialVersionUID = 1L;

    {
        counter = FnBooking.getMaxId(new Database().dbBooking);
    }

    public Booking(int userId, int scheduleLineID,  List<Passenger> passengerList) {
        this.id = ++counter;
        this.userId=userId;
        this.scheduleLineID = scheduleLineID;
        this.ticketSize = passengerList.size();
        this.passengerList = passengerList;
    }

    public Booking() {
    }

    public int getUserId() {
        return userId;
    }

    @Override
    public int getId() {
        return id;
    }


    public int getScheduleLine() {
        return scheduleLineID;
    }

    public List<Passenger> getPassengerList() {
        return passengerList;
    }

    public void setPassengerList(List<Passenger> passengerList) {
        this.passengerList = passengerList;
    }

    @Override
    public String toString() {
        return "Booking: "+ id + " " + passengerList.toString();
    }
}
