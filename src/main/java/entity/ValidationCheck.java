package entity;

public class ValidationCheck {
    public static boolean checkPassword(String s){
     return lengthValid(s) && containsCapital(s)
               && containsSmall(s) && containsSymbol(s)
               && containsNumber(s);
    }

    public static boolean lengthNameValid(String s){
        return s.length()>=3;
}

    public static boolean lengthValid(String s){
        return s.length()>=8;
    }

    public static boolean isCapital(char c) {
        return c >= 'A' && c <= 'Z';
    }

    public  static boolean isSmall(char c) {
        return c >= 'a' && c <= 'z';
    }

    public static boolean isNumber(char c) {
        return c >= '0' && c <= '9';
    }

    public static  boolean isSymbol(char c) {

        return c == '@' || c == '$' || c=='&' || c=='*' ;
    }

    public static boolean containsSmall(String s){
        for (Character c: s.toCharArray()) {
            if(isSmall( c)) return  true;
        }
        return  false;
    }

    public static boolean containsCapital(String s){
        for (Character c: s.toCharArray()) {
            if(isCapital( c)) return  true;
        }
        return  false;
    }

    public static boolean containsSymbol(String s){
        for (Character c: s.toCharArray()) {
            if(isSymbol(c)) return  true;
        }
        return  false;
    }

    public static boolean containsNumber(String s){
        for (Character c: s.toCharArray()) {
            if(isNumber( c)) return  true;
        }
        return  false;
    }
}
