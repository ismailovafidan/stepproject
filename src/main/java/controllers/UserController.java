package controllers;

import dao.UserDAO;
import entity.User;
import entity.Utilities;
import entity.ValidationCheck;
import services.UserService;

import java.util.Optional;
import java.util.Scanner;

public class UserController {
//RegisterController
    static Scanner in = new Scanner(System.in);

    public  void register(){
        System.out.println("-----Register -----");
        String name = Utilities.getInfo("name");
        String surname =Utilities.getInfo("surname");
        String username =Utilities.getInfo("username");
        Optional<String> password = Optional.empty();
        while((!password.isPresent()) || !ValidationCheck.checkPassword(password.get())){
            System.out.println("Please, enter your password: ");
            password =Optional.of(in.next());
            if(ValidationCheck.checkPassword(password.get())){
                continue;
            }
            else {
                System.out.println("Password incorrect. Minimum password requirements: at least 8 character, must have (A-Z),(a-z),(0-9),(~,!,@...)");

            }
        }
        final User user = new User(name, surname, username, password.get());
        UserService service = new UserService();
        service.register(user);
        System.out.println("You are in!");
        MainController mainController = new MainController();
        mainController.run();
    }

    //LoginController

    public Optional<User> user;
    UserDAO db = new UserDAO("user.txt");

    public void login() {
        String username = Utilities.getInfo("username");
        String password = Utilities.getInfo("password");
        UserService userService = new UserService();
        int login = userService.searchUser(username, password);
        checkLogin(login);
    }


    public void checkLogin(int login){
        if (login != -1) {
            user = db.get(login);
            System.out.println("Welcome to our app!!");
            LoginMain loginMain = new LoginMain(user);
            loginMain.loginMenu();
        } else {
            System.out.println("Username or password is incorrected, Please, try again");
            MainController mainController = new MainController();
            mainController.run();
        }
    }
}
