package controllers;

import entity.User;
import entity.Utilities;
import services.ScheduleService;
import java.util.Optional;

public class ScheduleController {
    ScheduleService scheduleService=new ScheduleService();
    public void showSchedule(){
        Utilities.showSchedule(scheduleService.getAllSchedules());
    }
    public void showMySchedule(Optional<User> user){
        Utilities.showMySchedule(scheduleService.getMySchedules(user));
    }

    public int sizeAllFlight(){
        return  scheduleService.getAllSchedules().size();
    }
}
