package controllers;

import entity.Passenger;
import entity.User;
import entity.Utilities;
import ex.FlightNotFoundException;
import services.BookingService;
import services.ScheduleService;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class BookingController {
    private Optional<User> user;
    ScheduleController scheduleController = new ScheduleController();
    Scanner in = new Scanner(System.in);

public BookingController(Optional<User> user) {
        this.user = user;
    }

    public boolean booking() {
        if (scheduleController.sizeAllFlight() == 0)
            throw new FlightNotFoundException("There is no any flight, please, try again later");
        scheduleController.showSchedule();
        String origin = getInfo("Please, enter your origin location");
        String destination = getInfo("Please, enter your destination location");
        String date = getInfo("Please, enter date (With this form : yyyy-mm-dd)");
        int index = checkBooking(origin, destination, date);
        if (index < 0)
            throw new FlightNotFoundException("There is no any Flight");
        String ticketSize = getInfo("Please enter Ticket size");
        List<Passenger> list = Utilities.checkTicketSize(ticketSize);
        return addBooking(index, list);
    }


    public int checkBooking(String origin, String destination, String date) {
        ScheduleService scheduleService = new ScheduleService();
        int check = scheduleService.scheduleCheck(origin, destination, date);
        return check;
    }

    public boolean addBooking(  int index, List<Passenger> list) {
        BookingService bookingService = new BookingService(this.user);
    return  bookingService.addBooking(this.user.get().getId(),index ,list );
    }

    public String getInfo(String text) {
        LoginMain loginMain = new LoginMain(user);
        System.out.println(text);
        String txt1 = in.next();
        if (txt1.equalsIgnoreCase("exit")) {
            loginMain.loginMenu();
        }
        return txt1;
    }

    public boolean cancelFlight() {
        BookingService bookingService = new BookingService(user);
        return bookingService.cancelFlight();
    }
}


