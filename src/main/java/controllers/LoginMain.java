package controllers;

import entity.Print;
import entity.User;
import ex.FlightNotFoundException;

import java.util.Optional;
import java.util.Scanner;

   public class LoginMain {
       private Optional<User> user;
       Print print = new Print();
       MainController mainController = new MainController();
       ScheduleController scheduleController=new ScheduleController();
       Scanner scanner = new Scanner(System.in);

       public LoginMain(Optional<User> user) {
           this.user = user;
       }

       public  void loginMenu(){
               print.printLoginMenu();

            boolean k=true;

            while (k){
                String input=scanner.nextLine();
                switch (input){
                    case "1":
                        showSchedule();
                        k=false;
                        break;
                    case "2":
                        booking();
                        k=false;
                        break;
                    case "3":
                        myFlight();
                        k=false;
                        break;
                    case "4":
                        cancelFlight();
                        k=false;
                        break;
                    case "5":
                        exit();
                        k=false;
                        break;


                }


        }
        }
            public void showSchedule(){
                    scheduleController.showSchedule();
                    loginMenu();
                }

            public void booking(){
                BookingController bookingController = new BookingController(user);
            try {
                if (bookingController.booking())
                    System.out.println("Schedule added successfully");
                    else System.out.println("Problem during adding schedule");
            } catch (FlightNotFoundException ex) {
                System.out.println("There is no any Flight");
            }
            loginMenu();
        }
    public void myFlight(){
       scheduleController.showMySchedule(this.user);
        loginMenu();
    }
    public void cancelFlight() {
        BookingController bookingController = new BookingController(user);
     bookingController.cancelFlight();
        loginMenu();

    }
    public void exit(){
      mainController.run();
      }



   }
