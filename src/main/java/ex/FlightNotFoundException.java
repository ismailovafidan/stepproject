package ex;

public class FlightNotFoundException extends RuntimeException {
    private String reason;

    public FlightNotFoundException(String reason) {
        super(reason);
        this.reason = reason;
    }
}
