package services;
import dao.Database;
import entity.*;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
public class ScheduleService {
    Database dao=new Database();

    public int scheduleCheck(String origin, String destination, String date){
        List<ScheduleLine> all=getAllSchedules();
        int check=-1;
        for(ScheduleLine scheduleLine:all){
            if (scheduleLine.getOriginString().equalsIgnoreCase(origin) &&
                    scheduleLine.getDestinationString().equalsIgnoreCase(destination) &&
                    scheduleLine.parseTime().equals(date)
            ) {
                check = scheduleLine.getId();

            }
        }
        return check;
    }
    public List<ScheduleLine> getAllSchedules(){
        return dao.dbSchedule.getAll();
    }

    public HashMap<Booking,ScheduleLine> getMySchedules(Optional<User> user){
        List <Booking> bookings = dao.dbBooking.getAllBy(booking -> booking.getUserId()==user.get().getId());
        List <ScheduleLine> scheduleLines = dao.dbSchedule.getAll();
        HashMap<Booking,ScheduleLine> myBooking=new HashMap<>();
        for (ScheduleLine schedule:scheduleLines) {
            for (Booking booking:bookings){
                if(schedule.getId()==booking.getScheduleLine()){
                    myBooking.put(booking, schedule);
                }
            }

        }
        return  myBooking;

    }




}
