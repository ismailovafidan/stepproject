package services;


import dao.Database;
import entity.*;
import java.util.*;

public class BookingService {
    private Optional<User> user;
    Scanner in = new Scanner(System.in);
    Database dao = new Database();

    public BookingService(Optional<User> user) {
        this.user = user;
    }

    public  boolean addBooking(int userId, int scheduleId, List<Passenger> passengerList){
     return dao.dbBooking.create(new Booking(userId,scheduleId,passengerList));
    }

    public boolean cancelFlight(){
        System.out.println("What do you want to cancel?");
        System.out.println("1-Passenger");
        System.out.println("2-Flight");
         switch (in.next()){
            case "1":
               return cancelPassenger();
               case "2":
              return  deleteFlight();
        }
          return false;
    }

    private boolean cancelPassenger() {
        System.out.println("Please, enter the id of Flight which you want to delete passenger of: ");
        int id = in.nextInt();
        Optional<Booking> booking = dao.dbBooking.get(id);
        if(booking.isPresent()){
            System.out.println("Please, enter the id of user you want to delete");
            int idPassenger = in.nextInt();
            List<Passenger> passengers = booking.get().getPassengerList();
            for (Passenger pass: passengers) {
                if (pass.getId()==idPassenger) {
                    passengers.remove(pass);
                    booking.get().setPassengerList(passengers);
                    dao.dbBooking.delete(booking.get().getId());
                    dao.dbBooking.create(booking.get());
                }
            }
        }
        return true;

    }

    public boolean deleteFlight(){
        System.out.println("Please enter the flight id you want to delete");
        int idNumber = in.nextInt();
        List<Booking> all = dao.dbBooking.getAll();
        for (Booking booking: all) {
            if(booking.getUserId() == user.get().getId() && booking.getId()==idNumber)
                return dao.dbBooking.delete(idNumber);
        }
        return false;
    }

}
