package services;

import dao.Database;
import entity.User;

import java.util.List;

public class UserService {
    Database dao=new Database();
    public  int searchUser(String username, String password){
        int login = -1;
        List<User> all = dao.dbUser.getAll();
        if(all.size()!=0){
            List<User> users = all;
            boolean log = false;
            for (User user: users) {
                if(user.getUsername().equals(username) && user.getPassword().equals(password)) {
                    login = user.getId();
                    log =true;
                }
            }
            if(!log) System.out.println("Please, enter username and password");
        }
        else System.out.println("Sorry, there is no one with this username and password");
        return login;
    }


    public void  register(User user){
        Database dao=new Database();
        dao.dbUser.create(user);
    }
}
