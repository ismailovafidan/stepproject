package controllers;

import entity.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookingControllerTest {

    private BookingController bookingController;
    private User user;
    private Passenger passenger;
    private ScheduleLine scheduleLine;
    private Booking booking;


    @BeforeEach
    public void setUp(){

        user=new User(1,"fidan","fidan");
        Passenger passenger1=new Passenger("sevinc","sevinc");
        Passenger passenger2=new Passenger("konul","konul");
        ScheduleLine scheduleLine=new ScheduleLine(1,LocalDateTime.of(2020,03,23,15,06),Cities.Baku,Cities.Istanbul);

    }



    @Test
    void booking() {
        bookingController.booking();
    }

    @Test
    void checkBooking() {
    }

    @Test
    void addBooking() {
    }

    @Test
    void getInfo() {
    }

    @Test
    void cancelFlight() {
    }
}